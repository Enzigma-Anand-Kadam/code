//Trailhead Assi 1
//Create an Apex trigger that sets an account’s Shipping Postal Code to match the Billing Postal Code 
//if the Match Billing Address option is selected. Fire the trigger before inserting an account or updating an account.

trigger AccountAddressTrigger on Account (before insert,before update,before delete,after insert) {

    if(Trigger.isBefore){ 
        if(Trigger.isInsert){  
            AccountTriggerHandler.matchShippingAddressToBillingAddress(Trigger.new); //callimg the handler method if its before insert/update
        		     // Trigger.new is passed as the data is before insert/update -> returns list of new version of sObject record
        }
        if(Trigger.isUpdate){  
            AccountTriggerHandler.matchShippingAddressToBillingAddress(Trigger.new); //calling the handler method if its before insert/update
        		     // Trigger.new is passed as the data is before insert/update -> returns list of new version of sObject record
        	
            AccountTriggerHandler.updateActive(Trigger.new, Trigger.oldMap); // Assignment 1 Q.2 Engineering to Finance 
        }
        if(Trigger.isDelete){
            AccountTriggerHandler.preventDelete(Trigger.old); //Assignment 1 Q.3 
            
    		AccountTriggerHandler.sendMail(Trigger.old);

        }
    }
    
    
    
    if(Trigger.isAfter){
        
        if(Trigger.isInsert){
            AccountTriggerHandler.createTask(Trigger.new); //Assignment 1 Q.4
        }
        
        if(Trigger.isUpdate){
           // AccountTriggerHandler.updateContact(Trigger.new,Trigger.oldMap);
        }
        
        if(Trigger.isDelete){
            AccountTriggerHandler.sendMail(Trigger.old);
        }
    }
}