public class AccountTriggerHandler {
    
    /* Assignment 1 -Question 1
* Write a trigger on Account, when record is inserted in the Account, 
* account billing address value should be automatically copied into the account shipping address 
* and industry field values to Engineering.
*/    
    public static void matchShippingAddressToBillingAddress(List<Account> accountlist){
        
        for(Account account : accountlist){      //for each loop to iterate through the list records
            if(account.Match_Billing_Address__c == true){   //checkbox is selected or not - Trailhead Task
                account.ShippingCity = account.BillingCity;
                account.ShippingCountry = account.BillingCountry;
                account.ShippingGeocodeAccuracy = account.BillingGeocodeAccuracy;
                account.ShippingLatitude = account.BillingLatitude;
                account.ShippingLongitude = account.BillingLongitude;
                account.ShippingPostalCode = account.BillingPostalCode;
                account.ShippingState = account.BillingState;
                account.ShippingStreet = account.BillingStreet;
            }
            account.Industry = 'Engineering';  //Assignment 3 - 1
        }
    }
    
    /*Assignment 1 - Question 2
* If the Account field -Industry is updated from Engineering to finance,then set Active field as true.
*/ 
    /* by using Trigger.new & Trigger.old
public static void updateActive(List<Account> accountNewList, List<Account> accountOldList){

for(Account oldAccount : accountOldList ){
if(oldAccount.Industry == 'Engineering' ){
for(Account newAccount : accountNewList){
if(newAccount.Industry == 'Finance')
newAccount.Active__c = 'Yes';
}
}
}
} */
    //By using Trigger.new,Trigger.oldMap
    public static void updateActive(List<Account> accountNewList, Map<Id,Account> mapAccount){
        
        for(Account objAccount : accountNewList){
            Account oldAccount = mapAccount.get(objAccount.Id);
            if(oldAccount.Industry=='Engineering')
                if(objAccount.Industry=='Finance')
                objAccount.Active__c = 'Yes';
        }
    }
    
    /*Question 3
If Account is Active,then prevent it from deletion. 
If Account has any opportunity associated with it prevent it fromdeletion.(Use addError() method).*/    
    
    public static void preventDelete(List<Account> listAccountToDelete){
        Map<Id, Account> mapAccountWithOpp = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN 
                                                                   (SELECT AccountId FROM Opportunity) AND 
                                                                   Id IN : listAccountToDelete]);
        for(Account account : listAccountToDelete){
            if(account.Active__c == 'Yes')
                account.addError('Active');
            
            if(mapAccountWithOpp.containsKey(account.Id))
                account.addError('Opportuinity');
        }
    }
    
    
    /*Question 4
* When a new account is created with annual revenue of more than 50,000 
* then a task should be created under Activity object.
*/    
    public static void createTask(List<Account> accountlist){
        List<Task> tasklist = new List<Task>();
        for(Account account : accountlist){
            Task task = new Task();
            if(account.AnnualRevenue > 50000 ){
                task.Subject = 'Created by account creation';
                task.WhatId = account.Id;
                tasklist.add(task);
            }
        }
        insert tasklist;
    }
    
    
    /*Q-5
Whenever an Account phone is modified then update Contact record with phone field 
(otherphone field with oldvalue and homephone field with new value) associated with Account.*/
    
    public static void updateContact(List<Account> newAccountList, Map<Id,Account> oldAccountMap){
        
        Map<Id,Account> accountContain = new Map<Id,Account>(); 		//map to save accounts whose phone is updated.
        
        for(Account account : newAccountList){       					//for each loop to iterate new account list
            Account oldAccount = oldAccountMap.get(account.Id);  		//saving before update account record id
            if(account.Phone != oldAccount.Phone){   					//checking if phone field is updated
                accountContain.put(account.Id, account);  				//Adding account record in map if its updated to later compare with contacts
            }
        }
        // List<Contact> contactList = [SELECT AccountId,Name,HomePhone,OtherPhone FROM Contact //fetching contact list related to account saved in map 
        //                              WHERE AccountId IN : accountContain.keySet() ]; 
        List<Contact> contactList = new List<Contact>();								//new contact list to update the contacts
        
        for(Contact contact : [ SELECT Id, AccountId, HomePhone, OtherPhone FROM Contact 
                               WHERE AccountId IN : accountContain.keySet()] ){ //fetching contact list related to account saved in map 
                                   
                                   //if created map contains the Account of the iterating contact accontId 
              contact.HomePhone = accountContain.get(contact.AccountId).Phone;			//update HomePhone
              contact.OtherPhone = oldAccountMap.get(contact.AccountId).Phone;			//Update OtherPhone
              contactList.add(contact);													//Add to list
                                   
                               }
        update contactList;	//update the list
    }
    
    
    /*Q-6
If Account is deleted, then send email (to appropriate email Id where the mail received can be 
checked).*/
    public static void sendMail(List<Account> deletedAccounts) {
        List<String> emailAddresses = new List<String>();
        for (Account account : deletedAccounts) {
            // add email addresses to list
            emailAddresses.add('anandkadam12345@yahoo.com');
            
        }
        if (!emailAddresses.isEmpty()) {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setToAddresses(emailAddresses);
            email.setSubject('Account Deleted');
            email.setPlainTextBody('An Account record has been deleted. Please check the details.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
    }
}